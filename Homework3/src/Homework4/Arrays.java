package Homework4;

import java.util.Scanner;

public class Arrays {
    public static void main(String[] args){
        int k = 5;
        for (int i = 0; i < 3; i++) {
            k = k -= 2;
            System.out.print(k);
            System.out.print(" ");
            k = k *=2;
            System.out.print(k);
            System.out.print(" ");
        }
        System.out.println("...");

        int attempt = 0;
        while (attempt<3){
            Scanner input = new Scanner(System.in);
            System.out.println("Введите следующее число последовательности");
            System.out.println("Осталось попыток ввода " + (3 - attempt));


            int sum = input.nextInt();
            if (sum == 10){
                System.out.println("Число отгадано!");
            }
            else {
                System.out.println("Неправильное число");
                attempt++;
            }

        }


    }
}
