package Calculator;

import java.util.Scanner;

public class Calculator {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите Ваше выражение в следующем формате: 1 + 1");
        try {
            double val = in.nextDouble();
            switch (in.next()) {
                case "+":
                    System.out.print("Результат: ");
                    System.out.print(val + in.nextDouble());
                    break;
                case "-":
                    System.out.print(val - in.nextDouble());
                    break;
                case "*":
                    System.out.print(val * in.nextDouble());
                    break;
                case "/":
                    System.out.print(val / in.nextDouble());
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Неправильный формат");
        }
    }
}